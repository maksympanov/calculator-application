package ua.edu.hneu.calculator;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;

/**
 * Provides basic calculator operations
 *
 * @author Maksym Panov
 * @version 1
 */
@Component
public class CalculatorOperations {

    public static final double DELTA = 0.0000001;

    public BigDecimal add(double a, double b) {
        var aDecimal = BigDecimal.valueOf(a);
        var bDecimal = BigDecimal.valueOf(b);
        return aDecimal.add(bDecimal);
    }

    public BigDecimal sub(double a, double b) {
        var aDecimal = BigDecimal.valueOf(a);
        var bDecimal = BigDecimal.valueOf(b);
        return aDecimal.subtract(bDecimal);
    }

    public BigDecimal multiply(double a, double b) {
        var aDecimal = BigDecimal.valueOf(a);
        var bDecimal = BigDecimal.valueOf(b);
        return aDecimal.multiply(bDecimal);
    }

    public BigDecimal divide(double a, double b) {
        if (abs(b) < DELTA) {
            throw new ArithmeticException("Cannot divide by zero!");
        }
        return BigDecimal.valueOf(a / b);
    }

    /**
     * Greatest Common Divisor lookup operation
     *
     * @return greatest common divisor of two numbers
     */
    public long gcd(long a, long b) {
        return a == 0 ? b : gcd(b % a, a);
    }

    /**
     * Least Common Multiple lookup operation
     *
     * @return least common multiple of two numbers
     */
    public long lcm(long a, long b) {
        return (a / gcd(a, b)) * b;
    }

    public List<Double> quadraticEquation(double a, double b, double c) {
        double d = b * b - 4 * a * c;
        if (d < 0) {
            return Collections.emptyList();
        }
        if (abs(d) < DELTA) {
            return List.of(-b / (2 * a));
        }
        return List.of((-b + sqrt(d)) / (2 * a), (-b - sqrt(d)) / (2 * a));
    }

}


