package ua.edu.hneu.calculator;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Scanner;

import static java.lang.System.out;
import static ua.edu.hneu.calculator.Operation.*;

@Service
@RequiredArgsConstructor
public class CalculatorService {

    private final CalculatorOperations calculatorOperations;
    private final Scanner IN = new Scanner(System.in);

    @Bean
    public void calculatorEventLoop() throws InterruptedException {
        while (true) {
            out.println("Choose an operation:");
            out.println("1 - add two numbers (a + b)");
            out.println("2 - subtract two numbers (a - b)");
            out.println("3 - multiply two numbers (a * b)");
            out.println("4 - divide two numbers (a / b)");
            out.println("5 - find greatest common divisor of two numbers (GCD)");
            out.println("6 - find lowest common multiple of two numbers (LCM)");
            out.println("7 - find roots of quadratic equation");
            out.println("8 - close the application (X)");

            try {
                var operation = Operation.getById(IN.nextInt());

                if (operation == EXIT) {
                    out.println("Exiting...\n\n");
                    break;
                }

                if (operation == DO_NOTHING)  {
                    out.println("Incorrect command!!!\n\n");
                    continue;
                }

                if (operation == GCD || operation == LCM) {
                    out.print("Enter first integer number: ");
                    var a = IN.nextLong();
                    out.print("Enter second integer number: ");
                    var b = IN.nextLong();

                    var result = switch (operation) {
                        case GCD -> calculatorOperations.gcd(a, b);
                        case LCM -> calculatorOperations.lcm(a, b);
                        default -> null;
                    };

                    out.printf("%s of these numbers: %s%n%n%n", operation.getHumanRead(), result);
                } else if (operation == QUADRATIC_EQUATION) {
                    out.print("Enter a: ");
                    double a = IN.nextDouble();
                    out.print("Enter b: ");
                    double b = IN.nextDouble();
                    out.print("Enter c: ");
                    double c = IN.nextDouble();

                    List<Double> result = calculatorOperations.quadraticEquation(a, b, c);
                    out.printf("%s are: %s%n%n%n", operation.getHumanRead(), result);
                } else {
                    out.print("Enter first real number: ");
                    var a = IN.nextDouble();
                    out.print("Enter second real number: ");
                    var b = IN.nextDouble();

                    var result = switch (operation) {
                        case ADDITION -> calculatorOperations.add(a, b);
                        case SUBTRACTION -> calculatorOperations.sub(a, b);
                        case MULTIPLICATION -> calculatorOperations.multiply(a, b);
                        case DIVISION -> calculatorOperations.divide(a, b);
                        default -> null;
                    };

                    out.printf("%s of these numbers: %s%n%n%n", operation.getHumanRead(), result);
                }
            } catch (Exception ignored) {
                out.println("Error in input occurred!");
                IN.nextLine();
            }

            // Just for UX
            Thread.sleep(1000);
        }
    }

}
